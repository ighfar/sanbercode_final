@extends('layouts.admin')

@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('Kategori') }}</h1>

    <!-- Begin Page Content -->
                <div class="container-fluid">

         
<a href="kategori/create" class="btn btn-primary">Tambah</a>
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
               <th scope="col" width="150">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($kategori as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama_category}}</td>
                       
                        <td>
                            <a href="/kategori/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/kategori/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/cast/{{$value->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>

                </div>
                <!-- /.container-fluid -->


@endsection
