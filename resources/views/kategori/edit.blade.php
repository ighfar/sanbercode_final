@extends('layouts.admin')
@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('Kategori') }}</h1>
        <div class="container-fluid">
<div>
     <h2>Edit Kategori {{$kategori->id}}</h2>
        <form action="/kategori/{{$kategori->id}}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="nama_category">Nama Kategori</label>
                <input type="text" class="form-control" name="nama_category" value="{{$kategori->nama_category}}" id="nama_category" placeholder="Masukkan Nama Kategori">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
         
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
</div>
</div>
@endsection