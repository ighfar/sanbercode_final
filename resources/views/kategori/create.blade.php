@extends('layouts.admin')
@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('Kategori') }}</h1>
        <div class="container-fluid">
<div>
    <h2>Tambah Data</h2>
        <form action="/kategori" method="POST">
            @csrf
            <div class="form-group">
                <label for="nama">Nama Kategori</label>
                <input type="text" class="form-control" name="nama_category" id="nama_category" placeholder="Masukkan Nama Kategori">
                @error('nama_category')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
         
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>
</div>
@endsection